package com.chikieblan4ik.plsxmlplssssssss;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "chikibombom1";
    private LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        linearLayout = findViewById(R.id.layout1);

        parseXML();
    }

    ViewGroup.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 80);
    ViewGroup.LayoutParams layoutParamsText = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

    public void parseXML() {
        Runnable run = new Runnable() {
            @Override
            public void run() {
                try {
                    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                    DocumentBuilder builder = factory.newDocumentBuilder();
                    Document document = builder.parse("http://www.cbr.ru/scripts/XML_daily.asp?date_req=20/09/2002");

                    NodeList valuteNodeList = document.getElementsByTagName("Valute");

                    String txt1;
                    String txt2;
                    String txt3;
                    String txt4;
                    String txt5;
                    for (int i = 0; i < valuteNodeList.getLength(); i++) {
                        NodeList childNodes = valuteNodeList.item(i).getChildNodes();
                        Log.i(TAG, childNodes.item(i).getTextContent());
                        for (int j = 0; j < childNodes.getLength(); j++) {
                            ConstraintLayout newConstraintLayout = new ConstraintLayout(MainActivity.this);
                            newConstraintLayout.setLayoutParams(layoutParams);
                            switch (childNodes.item(j).getNodeName()) {
                                case "NumCode":
                                    txt1 = childNodes.item(j).getTextContent();
                                    TextView textCodeN = new TextView(MainActivity.this);
                                    textCodeN.setLayoutParams(layoutParamsText);
                                    textCodeN.setText(txt1);
                                    newConstraintLayout.addView(textCodeN);
                                    break;
                                case "CharCode":
                                    txt2 = childNodes.item(j).getTextContent();
                                    TextView text3SymbN = new TextView(MainActivity.this);
                                    text3SymbN.setLayoutParams(layoutParamsText);
                                    text3SymbN.setText(txt2);
                                    newConstraintLayout.addView(text3SymbN);
                                    break;
                                case "Nominal":
                                    txt3 = childNodes.item(j).getTextContent();
                                    TextView textNominalN = new TextView(MainActivity.this);
                                    textNominalN.setLayoutParams(layoutParamsText);
                                    textNominalN.setText(txt3);
                                    newConstraintLayout.addView(textNominalN);
                                    break;
                                case "Name":
                                    txt4 = childNodes.item(j).getTextContent();
                                    TextView textNameN = new TextView(MainActivity.this);
                                    textNameN.setLayoutParams(layoutParamsText);
                                    textNameN.setText(txt4);
                                    newConstraintLayout.addView(textNameN);
                                    break;
                                case "Value":
                                    txt5 = childNodes.item(j).getTextContent();
                                    TextView textPriceN = new TextView(MainActivity.this);
                                    textPriceN.setLayoutParams(layoutParamsText);
                                    textPriceN.setText(txt5);
                                    newConstraintLayout.addView(textPriceN);
                                    break;
                                default:
                                    break;
                            }
                            linearLayout.addView(newConstraintLayout);
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.i(TAG, String.valueOf(e));
                }
            }
        };

        Thread th = new Thread(run);
        try {
            th.start();
            th.join();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}