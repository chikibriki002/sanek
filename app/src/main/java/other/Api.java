package other;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class Api {

    String rezult;

    private static final String TAG = "dinamik";

    // Ну тут и еблану понятно, что тут открываеца соединение. где тут? я тут. батут
    public HttpURLConnection startConnection(String url) {
        try {
            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            return connection;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    // Эта хуйня читает соединение и чисто данные хуячит в ссылку шобы я потом её бля выебал?
    public String responseFromStream(BufferedReader in) {
        StringBuilder response = new StringBuilder();
        String temp;
        try {
            while ((temp = in.readLine()) != null) {
                response.append(temp);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response.toString();
    }

    // Для POST я хуячу шобы ссылку сделало мне ну типа ссылка-запрос кайф
    public String makeLink(String[] keys, String[] values) {
        StringBuilder constructor = new StringBuilder();
        for (int i = 0; i < keys.length; i++) {
            constructor.append(keys[i]);
            constructor.append("=");
            constructor.append(values[i]);
            constructor.append("&");
        }
        return constructor.substring(0, constructor.length() - 1);
    }

    String get(final String url) {
        Runnable run = new Runnable() {
            @Override
            public void run() {
                try {
                    HttpURLConnection connection = startConnection(url);
                    BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    rezult = responseFromStream(in);
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.i(TAG, "Error in GET method: " + e);
                }
            }
        };

        Thread th = new Thread();
        try {
            th.start();
            th.join();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return rezult;
    }

    String post(final String[] keys, final String[] values, final String url) {
        Runnable run = new Runnable() {
            @Override
            public void run() {
                try {
                    HttpURLConnection connection = startConnection(url);
                    String link = makeLink(keys, values);

                    connection.setRequestMethod("POST");
                    connection.setDoOutput(true);
                    connection.getOutputStream().write(link.getBytes());

                    BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    rezult = responseFromStream(in);

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.i(TAG, "Error in POST method: " + e);
                }
            }
        };

        Thread th = new Thread(run);
        try {
            th.start();
            th.join();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rezult;
    }
}
